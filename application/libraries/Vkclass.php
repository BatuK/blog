<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Vkclass
{
	/*ID приложения*/
	public $appID;
	/*Защитный ключ приложения*/
	public $appSecret;
	/*Адресс для переадресации*/
	public $redirectUrl;
	/*Права приложения*/
	public $appScope;
	/*Версия API*/
	public $appApi;

	function __construct()
	{
		$this->appID = 4156828;
		$this->appSecret = 'nvwunixrvgE0ZjaamOIS';
		$this->redirectUrl = site_url().'admin/addposts';
		$this->appScope = 'offline';
		$this->appApi = '5.30';
	}

	public function getCode () 
	{
		$params = array
		(
			'client_id' => $this->appID,
			'scope' => $this->appScope,
			'redirect_uri' => $this->redirectUrl,
			'response_type' => 'code',
			'v' => $this->appApi
		);
		$url = 'https://oauth.vk.com/authorize?';
		return $url.http_build_query($params);
	}

	public function getToken ($code) 
	{
		$url = 'https://oauth.vk.com/access_token?';
		$params = array
		(
			'client_id'=> $this->appID, 
			'client_secret'=> $this->appSecret,
			'code' => $code,
			'redirect_uri' => $this->redirectUrl
		);
		$html = json_decode(file_get_contents($url.http_build_query($params)));
		return $html;
	}

	public function method($method, $params, $token = NULL)
	{
		$url = "https://api.vk.com/method/".$method;
		$params['access_token'] = $token;
		$parameters = http_build_query($params);
		$html = json_decode(file_get_contents($url.'?'.$parameters));
		return $html;
	}
}