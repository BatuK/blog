<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
    <!DOCTYPE html>
    <html>
    <head>
        <title>Kube Web Framework</title>
     
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
     
        <link rel="stylesheet" href="<?php echo base_url('assets/css/kube.min.css'); ?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/coolstile.css');?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>">
     
        <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/kube.min.js'); ?>"></script>
     
    </head>
    <body>
    <header>
    	<div class="units-row">
    	 <div class="unit-100 unit-centered">
			<h1 class="text-centered">Ваши запросы в Google и Yandex</h1>
		</div>
    	</div>
    </header>
    
	<section>
		<div class="units-row">
			<div class="unit-60 unit-centered">
			<?php foreach($data as $post): ?>
				<article>
					<div class="units-row">
						<div class="unit-100"><h2><?php echo $post->text; ?></h2></div>
							<div class="readmore unit-20"> <i class="fa fa-anchor"></i> <a href="<?php echo site_url($post->postID.'/'.$post->alias); ?>">Дай подсказку</a></div>
							<div class="comments unit-20"> <i class="fa fa-comments-o"></i> Комментариев: 12</div>
							<div class="views unit-20"> <i class="fa fa-search"></i> Просмотрели: 242</div>
							<div class="date unit-20"><i class="fa fa-clock-o"></i> <?php echo $post->timeSP; ?> </div>
					</div>
				</article>
			<?php endforeach; ?>
			<?php echo $this->pagination->create_links(); ?>
			</div>
		</div>
	</section>

    </body>
    </html>