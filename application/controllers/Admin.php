<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function index()
	{
		$this->load->view('admin/home');
	}

	public function addnew ()
	{
		$this->load->view('admin/newposts');
		/*Пытаемся разрешить пользователю добавлять статьи*/
		if($this->input->get('code'))
		{
			$token = $this->vkclass->gettoken($this->input->get('code'));
			$array = array(
				'token' => $token->access_token
			);
			$this->session->set_userdata($array);
			$query = $this->db->select('uid')->from('users')->where('uid', $token->user_id)->get();
			$user = $query->result();
			if(isset($user[0]))
			{
				$this->db->where('uid', $token->user_id);
				$this->db->update('users', array('token' => $token->access_token ));

			}else{
				$this->db->insert('users', array('uid'=>$token->user_id, 'token'=>$token->access_token));
			}
		}
	}

	public function ajaxgrab ()
	{
		$this->load->model('posts');
		$params = array(
			'count' => 100,
			'filter' => 'owner',
			'offset' => 0
			);
		$params['owner_id'] = $this->input->post('gid');
		$posts = $this->vkclass->method('wall.get', $params, $this->session->userdata('token'));
		for ($i=0; $i <= $posts->response[0]; $i+=100) { 
			$params = array(
			'count' => 100,
			'filter' => 'owner',
			'offset' => $i
			);		
			$params['owner_id'] = $this->input->post('gid');
			$posts = $this->vkclass->method('wall.get', $params, $this->session->userdata('token'));
			$this->posts->inPost($posts);
/*			unset($posts);
			unset($params);*/

		}
	}
	public function comments()
	{
		
	}
	public function allposts()
	{
		/*Достаём данные из базы*/
		$query = $this->db->select('id, text, timeSP')->from('posts')->limit(30, $this->uri->segment(3))->get();
		$data['posts'] = $query->result_array();

		/*Конфиг для пагинации*/
        $config['base_url'] = site_url('admin/allposts');
        $config['total_rows'] = $this->db->count_all('posts');
        $config['per_page'] = "30";
        $config["uri_segment"] = 3;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 5;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['first_link'] = 'First';
		$this->pagination->initialize($config);
		$this->load->view('admin/allposts', $data);
	}
}
