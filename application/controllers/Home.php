<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
class Home extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
	$this->load->model('fposts');
	$data['data'] = $this->fposts->getIndex($this->uri->segment(2));
	/*Пишем конфиг для пагинации*/
	$config['base_url'] = site_url();
        $config['total_rows'] = $this->db->count_all('posts');
        $config['per_page'] = "10";
        $config["uri_segment"] = 2;
        $config['num_links']= 8;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><span>';
        $config['cur_tag_close'] = '</span></li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['next_link'] = 'Влево <i class="fa fa-angle-double-right"></i>';
        $config['prev_link'] = '<i class="fa fa-angle-double-left"></i> Вправо ';
        $this->pagination->initialize($config);
        /*Подгружаем вьюху*/
		$this->load->view('homepage', $data);
	}

}