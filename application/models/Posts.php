<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* GetPostsModel
*/
class Posts extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function inPost($mass)
	{
		$this->db->trans_start();
		foreach ($mass->response as $key => $tobase) 
		{

		if($key == 0) continue;
		if(empty($tobase->text)) continue;
		$this->db->insert('posts', array(
			'postID'=>$tobase->id,
			'text'=>$tobase->text,
			'timeSP'=>date('Y-m-d H:i:s',$tobase->date),
			'publicID'=>$tobase->from_id,
			'type'=>'edit',
			'alias' => convert_accented_characters($tobase->text)
			));
		}
		$this->db->trans_complete();
	}
}