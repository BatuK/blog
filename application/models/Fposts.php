<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Модель для работы с постами на frontend
*/
class Fposts extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function getIndex($limit=NULL)
	{
		$query = $this->db->select('text, alias, timeSP,postID')->from('posts')->limit(10, $limit)->order_by('id', 'ASC')->get();
		return $query->result();
	}
}