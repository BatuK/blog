<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Menu Model
*/
class Menu extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function getmenu()
	{
		$query = $this->db->select('*')->from('menu')->get();
		return $query->result;
	}
}